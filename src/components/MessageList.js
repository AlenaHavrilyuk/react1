import React from 'react';
import Message from './Message';
import '../css/messageList.css'
import OwnMessage from './OwnMessage';
import Divider from './Divider';

class MessageList extends React.Component {

  messagesEndRef = React.createRef()

  componentDidMount () {
    this.scrollToBottom()
  }
  componentDidUpdate () {
    this.scrollToBottom()
  }
  scrollToBottom = () => {
    this.messagesEndRef.current.scrollIntoView({ behavior: 'smooth' })
  }

  formatDate = (d) => {
    var date;
    var d = new Date(d);
    const today = new Date();
    const yesterday = new Date(Date.now() - 86400000);
    if (d.getDate() === today.getDate() && d.getMonth() === today.getMonth() && d.getFullYear() === today.getFullYear()) {
      date = "today";
    } else if (d.getDate() === yesterday.getDate() && d.getMonth() === yesterday.getMonth() && d.getFullYear() === yesterday.getFullYear()) {
      date = "yesterday";
    } else 
    if (d.getFullYear() === today.getFullYear()) {
      date = new Intl.DateTimeFormat('en', { weekday: 'long', day: 'numeric', month: 'long' }).format(d);
    } else {
      date = new Intl.DateTimeFormat('en', { weekday: 'long', day: 'numeric', month: 'long', year: 'numeric' }).format(d);
    }
    return date;
  }

  render() {
    const msgList = [];
    var prevMessageDate = null;

    this.props.messages.forEach((msg) => {
      
      var date = this.formatDate(msg.createdAt);


      if (date !== prevMessageDate) {
        msgList.push(
          <Divider date={date} />
        );
      };
      prevMessageDate = date;

      if (msg.userId === "my") {
        msgList.push(
          <OwnMessage
            key={msg.id}
            message={msg}
            onUpdateMessages={this.props.onUpdateMessages}
            messages={this.props.messages}
            inputNewText={this.props.inputNewText}
          />
        );
      } else {
        msgList.push(
          <Message
            key={msg.id}
            message={msg}
          />
        );
      }
    })

    return (
      <div className="message-list">
        {msgList}
        <div className="messages-end" ref={this.messagesEndRef} />
      </div>
    )

  }
}

export default MessageList;