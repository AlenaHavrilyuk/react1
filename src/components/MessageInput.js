import React from 'react';
import '../css/messageInput.css';

class MessageInput extends React.Component{
    constructor(props){
        super(props); 
        this.state = {value: ""};
        this.handleChange = this.handleChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);    
        this.isMessageEdited = false; 
    }
    onSubmit(event) {
        var updatedMessages = this.props.messages;

        if (this.isMessageEdited) {
            updatedMessages.forEach(element => {
                if (element.id === this.props.editedMessage.id) {
                    element.text = this.state.value;
                    element.editedAt = new Date();
                }
            });
            this.state.value = "";
            this.props.closeEdition();
            this.isMessageEdited = false;
            this.props.onUpdateMessages(updatedMessages);
            event.preventDefault();
        } else{
        
        updatedMessages.push({
            "id": this.ID(),
            "userId": "my",
            "avatar": "",
            "user": "Me",
            "text": this.state.value,
            "createdAt": new Date(),
            "editedAt": ""
        })
        this.state.value = "";
        this.props.onUpdateMessages(updatedMessages);
        event.preventDefault();
    }
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    ID = function () {
        return '_' + Math.random().toString(36).substr(2, 9);
    }

    render(){
        if (this.props.editedMessage !== '' && !this.isMessageEdited) {
            this.isMessageEdited = true;
            this.state.value = this.props.editedMessage.text;
        }
        return(
        <form className="message-input" onSubmit={this.onSubmit}>
            <input type="text" value={this.state.value} onChange={this.handleChange}></input>
            <button type="submit">Send</button>
        </form>
        )
    }
}

export default MessageInput;