import React from 'react';
import '../css/header.css';

class Header extends React.Component{
    render(){

        let d = new Date(this.props.lastMessageDate);
        let da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d);
        let mo = new Intl.DateTimeFormat('en', { month: '2-digit' }).format(d);
        let ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);
        let ho = new Intl.DateTimeFormat('en', { hour: '2-digit', hour12: false}).format(d);
        let mi = new Intl.DateTimeFormat('en', { minute: '2-digit' }).format(d);
        let date = (`${da}.${mo}.${ye} ${ho}:${mi}`);
        return(
        <div className="header">
            <p className="header-title">
            My chat
            </p>
            <p className="header-users-count">
            {this.props.usersCount} participants
            </p>
            <p className="header-messages-count">
            {this.props.messagesCount} messages
            </p>
            <p className="header-last-message-date">
            last message at: {date}
            </p>
        </div>
        )
    }
}

export default Header;