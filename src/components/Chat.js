import React, { Component } from 'react';
import Header from './Header';
import MessageList from './MessageList';
import Preloader from './Preloader';
import '../css/chat.css'
import MessageInput from './MessageInput';


class Chat extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            messages: []
        } 
        this.isLoading = true; 
        this.editedMessage = ''; 
        this.updateMessages = this.updateMessages.bind(this);
        this.inputNewText = this.inputNewText.bind(this);
        this.closeEdition = this.closeEdition.bind(this);
    }

    componentDidMount() {
        fetch(this.props.url)
        .then(response => response.json())
        .then(data => {this.setState({messages: data})})
        .then(this.isLoading = false)        
    }

    updateMessages = (newMessages) => {
        this.setState({messages: newMessages});
    }

    inputNewText = (messageId) => {
        this.state.messages.forEach((element) => {
            if (element.id === messageId) {
                this.editedMessage = {text: element.text, id: messageId};
                this.forceUpdate();
            }
        }
        )
    }

    closeEdition = () => {
        this.editedMessage = '';
        this.forceUpdate();
    }

    render(){
        var data = this.state.messages;
        var messCount = data.length;
        if(this.isLoading){
            return (
                <div className="chat">
                    <Preloader />
                </div>
            )
        }
        else{ 
            var userSet = new Set();
            data.forEach(function(item) {
                userSet.add(item.userId);
            });
            var userCount = userSet.size;
            var lastMessDate = 
            data[data.length - 1].editedAt === "" ? 
            data[data.length - 1].createdAt :
            data[data.length - 1].editedAt;

            return (
                <div className="chat">
                    <Header messagesCount={messCount} usersCount={userCount} lastMessageDate={lastMessDate}/>
                    <MessageList messages={data} onUpdateMessages={this.updateMessages} inputNewText={this.inputNewText}/> 
                    <MessageInput onUpdateMessages={this.updateMessages} messages={data} editedMessage={this.editedMessage} closeEdition={this.closeEdition}/>
                </div>
            )
        }   
    }
}

export default Chat;