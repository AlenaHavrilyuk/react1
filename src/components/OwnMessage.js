import React from 'react';

class OwnMessage extends React.Component{
    constructor(props){
        super(props); 
        this.editMessage = this.editMessage.bind(this);
        this.deleteMessage = this.deleteMessage.bind(this);         
    }

    editMessage(){
        this.props.inputNewText(this.props.message.id);
    }

    deleteMessage(){
        var updatedMessages = this.props.messages;
        for( var i = 0; i < updatedMessages.length; i++){ 
            if ( updatedMessages[i].id === this.props.message.id) { 
                updatedMessages.splice(i, 1); 
            }
        
        }
        this.props.onUpdateMessages(updatedMessages);
    }

    render(){
        var data = this.props.message;
        var edited = "";
        var d;
        if(data.editedAt === "") {
            d = new Date(data.createdAt);
        } else{
            d = new Date(data.editedAt);
            edited = "edited";
        }
        let time = new Intl.DateTimeFormat('en', { hour: '2-digit', minute: '2-digit', hour12: false}).format(d);
        return(
        <div className="own-message">
            <div className="message-text">
                {data.text}
            </div>
            <div className="message-time">
                {time} {edited}
            </div>
            <button className="message-edit" onClick={this.editMessage}>
                edit
            </button>
            <button className="message-delete" onClick={this.deleteMessage}>
                delete
            </button>
            <div className="message-background"></div>
        </div>
        )
    }
}

export default OwnMessage;